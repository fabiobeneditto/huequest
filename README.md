# Huequest

## About it

Huequest is a simple HTTP Request and Response Service, inspired by [Httpbin](https://httpbin.org/)/[Mockbin](https://mockbin.org/), and powered by [Azion Cells](https://www.azion.com/en/blog/the-next-generation-in-serverless-azion-cells/)!

### Endpoints

So far, Huequest has the following endpoints:

| URI         | Description                                                                      |
|-------------|----------------------------------------------------------------------------------|
| /           | Returns a Hello world with a description of the project                          |
| /everything | Returns every possible detail about the request sent                             |
| /form       | Returns an HTML Form for testing purposes.                                       |
| /joke       | Returns a random joke (powered by [icanhazdadjoke](https://icanhazdadjoke.com/)) |


## Test it

You can find a running demo of Huequest at: [o72cj3e0vu.map.azionedge.net](http://o72cj3e0vu.map.azionedge.net/)
If you plan to use this as your Edge Application's Origin, don't forget to use `o72cj3e0vu.map.azionedge.net` as the Host Header.