"use strict";

const version = "2023.07.25";

const baseHeaders = {
  accept: "application/json",
  "content-type": "application/json",
  "x-version": version
};


/**
 * Main function to handle requests and validate JWT
 *
 * @async
 * @param {object} event - The incoming event object
 */
async function handleRequest(event) {
  const url = new URL(event.request.url);

  switch (true) {
  case url.pathname === "/":
    return new Response(
      JSON.stringify({
        hello: "World!",
        about: "Huequest is a simple HTTP Request and Response Service, inspired by Httpbin/Mockbin, and powered by Azion Cells"
      }, null, 2),
      {
        headers: baseHeaders,
        status: 200
      }
    );

  case url.pathname === "/joke":
    try {
      let jokeResponse = await fetch(
        "https://icanhazdadjoke.com/",
        {
          headers: baseHeaders
        }
      );
      let jokeJson = await jokeResponse.json();

      return new Response(JSON.stringify({joke: jokeJson.joke, id: jokeJson.id}, null, 2), {
        headers: baseHeaders,
        status: 200
      });
    } catch(err) {
      event.console.log("[Failed to retrieve a joke]", err.stack);

      return new Response(
        JSON.stringify({
          error: "Failed to retrieve a new joke."
        }, null, 2),
        {
          headers: baseHeaders,
          status: 500
        }
      );
    }
    break;

  case url.pathname.startsWith("/everything"):
    let now = Date.now();
    let currentDate = new Date(now);

    let requestContentType = event.request.headers.get("content-type");

    let body = {};
    let rawBody = "";

    if (event.request.body) {
      try {
        let requestClone = event.request.clone();
        rawBody = await requestClone.text();

        switch (true) {
        case requestContentType.includes("application/x-www-form-urlencoded"):
        case requestContentType.includes("multipart/form-data"):
          let requestFormData = await event.request.formData();
          body = Object.fromEntries(requestFormData);
          break;

        case requestContentType.includes("application/json"):
          body = await event.request.json();
          break;
        }
      } catch (err) {
        event.console.log("[Process request body]", err.stack);
      }
    }

    let everything = {
      timestamp: now,
      date: currentDate.toISOString(),
      url: event.request.url,
      uri: url.pathname,
      method: event.request.method,
      queryArgs: Object.fromEntries(url.searchParams),
      data: body,
      headers: Object.fromEntries(event.request.headers),
      metadata: event.request.metadata,
      rawData: {
        querystring: url.search,
        body: rawBody
      }
    };

    return new Response(
      JSON.stringify(everything, null, 2),
      {
        headers: baseHeaders,
        status: 200
      }
    );

  case url.pathname === "/form":
    let html = `
<!DOCTYPE html>
<html lang="en">
<head>
  <title>HTML Form</title>
  <link rel="icon" href="data:;base64,iVBORw0KGgo=">
</head>
<body>
<h1>HTML Form</h1>
<form action="/everything" method="post">
  <label for="first_name">First name:</label><br>
  <input type="text" id="first_name" name="first_name" value="John" placeholder="First name"><br><br>
  <label for="last_name">Last name:</label><br>
  <input type="text" id="last_name" name="last_name" value="Johnson" placeholder="Last name"><br><br>
  <input type="submit" value="Submit">
</form>
</body>
</html>
`;

    return new Response(
      html,
      {
        headers: {
          "content-type": "text/html",
          "x-version": version
        },
        status: 200
      }
    );

  default:
    return new Response(JSON.stringify({error: "Page not found."}, null, 2), {
      headers: baseHeaders,
      status: 404
    });
  }
}


/**
 * Add an event listener for 'fetch' events
 * Respond to the fetch event with the result of the handleRequest function
 */
addEventListener("fetch", event => {
  return event.respondWith(handleRequest(event));
});
